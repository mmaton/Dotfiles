# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh
setopt histignorespace
# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="pygmalion"

#while sleep 1;do tput sc;tput cup 0 $(($(tput cols)-29));date;tput rc;done & 

# Example aliases
alias zshconfig="vi ~/.zshrc" 
alias cisco="minicom -c on -b 9600"
alias 3com="minicom -c on -b 19200"
alias icurl="curl -I"
alias aftp="ftp anonymous:anonymous@$1"
alias simulator="open /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/Applications/iPhone\ Simulator.app"
alias svn="colorsvn"
alias infssh="ssh -i ~/.ssh/OpenReplyUKInfrastructure.PEM ubuntu@46.51.182.245"
alias madssh="ssh -i ~/.ssh/MADeployer-new.pem ec2-user@54.170.49.211"
alias infscp="scp -i ~/.ssh/OpenReplyUKInfrastructure.PEM"
alias cleardownloads="sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"
alias nginx="sudo nginx"
#alias ./configure="grc ./configure"
alias make="grc make"
alias startfpm="sudo php-fpm -c /usr/local/etc/php.ini &"
 #alias ohmyzsh="vi ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
 DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=1

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git brew tmux colorize colored-man screen history osx web-search)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/usr/local/sbin:/sbin:/opt/X11/bin:/usr/local/MacGPG2/bin
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/lib/node:/usr/local/lib/node_modules:/usr/local/share/npm/bin:/usr/local/share/npm/lib/node_modules:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/Users/landonschropp/.rbenv/shims:/usr/local/sbin:/usr/local/lib/node:/usr/local/lib/node_modules:/usr/local/share/npm/bin:/usr/local/share/npm/lib/node_modules:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin

export MSF_DATABASE_CONFIG=/usr/local/share/metasploit-framework/database.yml
source `brew --prefix grc`/etc/grc.bashrc
bindkey -e
bindkey '[C' forward-word
bindkey '[D' backward-word
alias tmux="TERM=screen-256color-bce tmux" 

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
alias apt-get=brew
export CODAPATH="/Applications/Coda 2.app"
  . `brew --prefix`/etc/profile.d/z.sh
export ANDROID_HOME=/usr/local/opt/android-sdk

PERL_MB_OPT="--install_base \"/Users/mmaton/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/Users/mmaton/perl5"; export PERL_MM_OPT;
